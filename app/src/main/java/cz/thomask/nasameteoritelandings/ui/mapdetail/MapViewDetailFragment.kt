package cz.thomask.nasameteoritelandings.ui.mapdetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import cz.thomask.nasameteoritelandings.R
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import cz.thomask.nasameteoritelandings.databinding.MapviewFragmentBinding
import cz.thomask.nasameteoritelandings.getApplicationComponent
import cz.thomask.nasameteoritelandings.ui.Utils.MeteoriteLandingDataEntryUIWrapper
import timber.log.Timber
import javax.inject.Inject


class MapViewDetailFragment : Fragment(), OnMapReadyCallback {
    private val args: MapViewDetailFragmentArgs by navArgs()

    @Inject
    lateinit var meteoritesRepository: MeteoriteLandingsRepository


    val viewModel: MapViewDetailViewModel by viewModels {
        MapViewDetailViewModelFactory(meteoritesRepository)
    }

    // has to be nullable for earlier release of resources to prevent leaking
    private var binding: MapviewFragmentBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = MapviewFragmentBinding.inflate(inflater, container, false).also {
            it.mapView.onCreate(savedInstanceState)
            it.mapView.getMapAsync(this)
        }
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // can be done by directly sending needed values in arguments or serialization to arguments bundle to get rid of nullability
        viewModel.queryMeteoriteLandingInfoByID(args.meteoriteId)?.let {
            Navigation.findNavController(view).currentDestination!!.label = provideCurrentScreenTitle(it)
            activity!!.title = provideCurrentScreenTitle(it)
        } ?: run {
            // somehow data is no longer available, just go back
            Navigation.findNavController(view).popBackStack()
        }
    }

    override fun onMapReady(gmap: GoogleMap) {
        // map is now ready, query data
        val meteoriteData = viewModel.queryMeteoriteLandingInfoByID(args.meteoriteId)
        meteoriteData?.let {
            if (it.reclat != null && it.reclon != null) {
                // show custom marker with info view and summary text,  move map to chosen location and zoom slightly
                val meteoriteLandingPosition = LatLng(it.reclat!!, it.reclon!!)
                val info = prepareMeteoriteLandingMarkerSnippetText(it)
                val markerOptions = MarkerOptions().apply {
                    title(it.name)
                    icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_meteorite))
                    snippet(info)
                    draggable(false)
                    position(meteoriteLandingPosition)

                }
                val marker = gmap.addMarker(markerOptions)
                marker.showInfoWindow()
                gmap.moveCamera(CameraUpdateFactory.zoomTo(4F))
                gmap.moveCamera(CameraUpdateFactory.newLatLng(meteoriteLandingPosition))
            } else {
                Timber.d("Null values in position of meteorite landing")
            }
        } ?: run {
            Timber.d("Null data for ID")
        }
    }

    private fun prepareMeteoriteLandingMarkerSnippetText(landingInfo: MeteoriteLandingDataEntry): String {
        val formatString = getString(R.string.landing_marker_snippet_text_format)
        val landingInfoUiWrapped = MeteoriteLandingDataEntryUIWrapper(landingInfo, requireContext())
        return formatString.format(landingInfoUiWrapped.getMassAsString(), landingInfoUiWrapped.getYearAsJustYearString())
    }

    private fun provideCurrentScreenTitle(it: MeteoriteLandingDataEntry): String {
        return it.name
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context.getApplicationComponent().inject(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding!!.mapView.onDestroy()
        binding = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding!!.mapView.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        binding!!.mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        binding!!.mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        binding!!.mapView.onStop()
    }

    override fun onPause() {
        binding!!.mapView.onPause()
        super.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding!!.mapView.onLowMemory()
    }

    class MapViewDetailViewModelFactory(val repository: MeteoriteLandingsRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MapViewDetailViewModel(repository) as T
        }

    }

}