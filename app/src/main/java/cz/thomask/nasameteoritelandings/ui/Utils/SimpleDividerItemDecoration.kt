package cz.thomask.nasameteoritelandings.ui.Utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration


/**
 * Recyclerview item decoration - drawing a line at the bottom of item
 */
class SimpleDividerItemDecoration(context: Context, drawableRes: Int, paddings: Int) : ItemDecoration() {
    private var mDivider: Drawable?
    private var mPaddings = paddings
    var height = 1


    init {
        val typedValue = TypedValue()
        val theme: Resources.Theme = context.theme
        theme.resolveAttribute(drawableRes, typedValue, true)
        mDivider = ColorDrawable(typedValue.data)
        height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1F, context.resources.getDisplayMetrics()).toInt()
    }


    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight
        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin
            val bottom = top + height
            mDivider!!.setBounds(left + mPaddings, top, right - mPaddings, bottom)
            mDivider!!.draw(c)
        }
    }
}