package cz.thomask.nasameteoritelandings.ui.main

import androidx.lifecycle.ViewModel
import cz.thomask.nasameteoritelandings.RealmLiveData
import cz.thomask.nasameteoritelandings.RealmProvider
import cz.thomask.nasameteoritelandings.asLiveData
import cz.thomask.nasameteoritelandings.data.DataFilter
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import io.realm.Realm

class LandingsListViewModel(val repository: MeteoriteLandingsRepository) : ViewModel() {
    var realm: Realm = RealmProvider.provideNewRealmInstance()
    fun retrieveLandingsList(): RealmLiveData<MeteoriteLandingDataEntry> {
        return repository.queryDatabaseForFilteredData(DataFilter.SINCE_2011, realm).asLiveData()
    }

    override fun onCleared() {
        super.onCleared()
        realm.close()
    }
}
