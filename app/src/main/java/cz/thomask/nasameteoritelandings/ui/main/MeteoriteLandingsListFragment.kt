package cz.thomask.nasameteoritelandings.ui.main

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.thomask.nasameteoritelandings.Constants
import cz.thomask.nasameteoritelandings.MeteoriteLandingsAutoUpdater
import cz.thomask.nasameteoritelandings.R
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import cz.thomask.nasameteoritelandings.databinding.MainFragmentBinding
import cz.thomask.nasameteoritelandings.databinding.MeteoritesRecyclerItemBinding
import cz.thomask.nasameteoritelandings.getApplicationComponent
import cz.thomask.nasameteoritelandings.ui.Utils.SimpleDividerItemDecoration
import timber.log.Timber
import javax.inject.Inject

class MeteoriteLandingsListFragment : Fragment() {

    @Inject
    lateinit var meteoritesRepository: MeteoriteLandingsRepository

    val viewModel: LandingsListViewModel by viewModels {
        LandingsListViewModelFactory(meteoritesRepository)
    }

    private lateinit var recyclerAdapter: MeteoritesListRecyclerviewAdapter

    // has to be nullable for earlier release of resources to prevent leaking
    private var binding: MainFragmentBinding? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showEmptyView()
        recyclerAdapter = MeteoritesListRecyclerviewAdapter(requireActivity().layoutInflater)

        binding!!.apply {
            emptyLabel.text = getString(R.string.empty_view_label_landings_database_empty)
            meteoritesListRecycler.layoutManager = LinearLayoutManager(activity!!)
            meteoritesListRecycler.adapter = recyclerAdapter
            meteoritesListRecycler.addItemDecoration(SimpleDividerItemDecoration(context!!, R.attr.colorDivider, 100))
        }

        viewModel.retrieveLandingsList().observe(viewLifecycleOwner, Observer { realmResults ->
            recyclerAdapter.submitList(realmResults)

            if (recyclerAdapter.itemCount == 0) {
                showEmptyView()
            } else {
                hideEmptyView()
            }
        })
        checkAndShowInfoDialogAfterFreshInstallation()
    }


    private fun showEmptyView() {
        binding!!.apply {
            meteoritesListRecycler.visibility = View.GONE
            emptyLabel.visibility = View.VISIBLE
        }
    }

    private fun hideEmptyView() {
        binding!!.apply {
            meteoritesListRecycler.visibility = View.VISIBLE
            emptyLabel.visibility = View.GONE
        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context.getApplicationComponent().inject(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.reload_menu_button -> {
                MeteoriteLandingsAutoUpdater.updateDatabaseImmediately(context!!.applicationContext)
                return true
            }
            R.id.info_dialog_menu_button -> {
                findNavController().navigate(R.id.action_mainFragment_to_welcomeScreenFragment)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkAndShowInfoDialogAfterFreshInstallation() {
        // at first database is empty, update once after fresh install so that database will get ready for first use
        val preferences = requireContext().getApplicationComponent().defaultPreferences()
        if (preferences.getBoolean(Constants.PREF_KEY_SHOWED_WELCOME_SCREEN, false) == false) {
            preferences.edit {
                putBoolean(Constants.PREF_KEY_SHOWED_WELCOME_SCREEN, true)
            }
            Timber.d("show fragment welcome after install")
            findNavController().navigate(R.id.action_mainFragment_to_welcomeScreenFragment)
        }
    }

    class MeteoriteRecyclerViewHolder(item: MeteoritesRecyclerItemBinding) : RecyclerView.ViewHolder(item.root) {
        val mass: TextView = item.meteoritesListItemMass
        val name: TextView = item.meteoritesListItemName
        val date: TextView = item.meteoritesListItemFallDate
    }

    internal class MeteoritesListRecyclerviewAdapter(val inflater: LayoutInflater) :
        ListAdapter<MeteoriteLandingDataEntry, MeteoriteRecyclerViewHolder>(MeteoritesListDiffCallback) {
        val viewBinder = MeteoriteLandingsRecyclerAdapterItemViewBinder()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeteoriteRecyclerViewHolder {
            return MeteoriteRecyclerViewHolder(MeteoritesRecyclerItemBinding.inflate(inflater, parent, false))
        }


        override fun onBindViewHolder(holder: MeteoriteRecyclerViewHolder, position: Int) {
            val meteoriteData = getItem(position)
            viewBinder.bindViews(holder, meteoriteData)

            // combination of ListAdapter and its detection of changes causes that objects in DB wont be replaced, in combination
            // with realm it can cause problems because onClickListeners will point to objects from previous query and will crash without
            // copy to final variables
            val meteoriteID = meteoriteData.id
            val meteoriteName = meteoriteData.name
            holder.itemView.setOnClickListener {
                val action = MeteoriteLandingsListFragmentDirections.showDetailMapViewAction(meteoriteID, meteoriteName)
                it.findNavController().navigate(action)
            }
        }

        object MeteoritesListDiffCallback : DiffUtil.ItemCallback<MeteoriteLandingDataEntry>() {
            override fun areItemsTheSame(oldItem: MeteoriteLandingDataEntry, newItem: MeteoriteLandingDataEntry): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MeteoriteLandingDataEntry, newItem: MeteoriteLandingDataEntry): Boolean {
                return oldItem.equalsContent(newItem)
            }

        }

    }

    class LandingsListViewModelFactory(val repository: MeteoriteLandingsRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LandingsListViewModel(repository) as T
        }

    }
}
