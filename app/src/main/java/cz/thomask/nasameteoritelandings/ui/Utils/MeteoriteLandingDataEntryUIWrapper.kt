package cz.thomask.nasameteoritelandings.ui.Utils

import android.content.Context
import cz.thomask.nasameteoritelandings.R
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.formatAndRoundToDigits
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class MeteoriteLandingDataEntryUIWrapper(val landingInfo: MeteoriteLandingDataEntry, val context: Context) {


    fun getMassAsString(): String {
        if (landingInfo.mass == null) {
            return context.getString(R.string.landing_info_mass_unknown)
        }
        return if (landingInfo.mass!! >= 1000) {
            "${(landingInfo.mass!! / 1000).formatAndRoundToDigits(3)}kg"
        } else {
            "${landingInfo.mass!!.formatAndRoundToDigits(3)}g"
        }
    }

    fun getYearAsJustYearString(): String {
        return if (landingInfo.year != null) {
            dateFromatter.format(Instant.ofEpochMilli(landingInfo.year!!))
        } else {
            context.getString(R.string.landing_info_date_unknown)
        }
    }

    companion object {
        private val dateFromatter = DateTimeFormatter.ofPattern("YYYY").withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault())
    }
}

