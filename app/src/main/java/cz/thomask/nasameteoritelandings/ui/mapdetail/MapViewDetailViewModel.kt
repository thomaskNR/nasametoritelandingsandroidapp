package cz.thomask.nasameteoritelandings.ui.mapdetail

import androidx.lifecycle.ViewModel
import cz.thomask.nasameteoritelandings.RealmProvider
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import io.realm.Realm

class MapViewDetailViewModel(val repository: MeteoriteLandingsRepository) : ViewModel() {
    var realm: Realm = RealmProvider.provideNewRealmInstance()
    fun queryMeteoriteLandingInfoByID(id: String): MeteoriteLandingDataEntry? {
        return repository.queryMeteoriteLandingInfoByID(id, realm)
    }

    override fun onCleared() {
        super.onCleared()
        realm.close()
    }

}