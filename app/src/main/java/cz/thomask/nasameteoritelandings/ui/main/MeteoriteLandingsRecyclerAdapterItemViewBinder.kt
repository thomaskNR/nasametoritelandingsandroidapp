package cz.thomask.nasameteoritelandings.ui.main

import cz.thomask.nasameteoritelandings.R
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.ui.Utils.MeteoriteLandingDataEntryUIWrapper

class MeteoriteLandingsRecyclerAdapterItemViewBinder {

    fun bindViews(viewHolder: MeteoriteLandingsListFragment.MeteoriteRecyclerViewHolder, meteoriteData: MeteoriteLandingDataEntry) {
        val localContext = viewHolder.itemView.context
        val meteoriteDataUiWrapped = MeteoriteLandingDataEntryUIWrapper(meteoriteData, localContext)
        viewHolder.name.text = meteoriteData.name
        viewHolder.date.text =
            localContext.getString(R.string.landings_list_row_landing_year).format(meteoriteDataUiWrapped.getYearAsJustYearString())
        viewHolder.mass.text =
            localContext.getString(R.string.landings_list_row_landing_mass).format(meteoriteDataUiWrapped.getMassAsString())

    }
}