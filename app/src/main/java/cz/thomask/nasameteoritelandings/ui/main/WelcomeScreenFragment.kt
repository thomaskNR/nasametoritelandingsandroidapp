package cz.thomask.nasameteoritelandings.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import cz.thomask.nasameteoritelandings.R
import cz.thomask.nasameteoritelandings.databinding.FragmentWelcomeScreenBinding
import timber.log.Timber

class WelcomeScreenFragment : DialogFragment() {

    var binding: FragmentWelcomeScreenBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentWelcomeScreenBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.dismissButton.setOnClickListener { dismiss() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}