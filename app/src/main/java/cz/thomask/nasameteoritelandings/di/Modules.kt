package cz.thomask.nasameteoritelandings.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class PreferencesModule(val context: Context) {

    @Singleton
    @Provides
    fun defaultPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
    }
}

@Module
object RealmConfigurationModule {

    @Singleton
    @Provides
    fun defaultRealmConfiguration() = RealmConfiguration.Builder()
        .deleteRealmIfMigrationNeeded()
        .schemaVersion(1)
        .compactOnLaunch { totalBytes, usedBytes ->
            // database file can get bigger when working with big number of entries, reclaim the space
            (totalBytes > 1024 * 1024 * 5) && (usedBytes.toDouble() / totalBytes.toDouble() < 0.7)
        }
        .build()
}

@Module
class OkHttpClientModule {

    @Singleton
    @Provides
    fun defaultOkHttpClient() = OkHttpClient()
}

