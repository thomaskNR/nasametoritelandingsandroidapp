package cz.thomask.nasameteoritelandings.di

import android.content.SharedPreferences
import cz.thomask.nasameteoritelandings.MainActivity
import cz.thomask.nasameteoritelandings.MeteoriteLandingsApplication
import cz.thomask.nasameteoritelandings.MeteoriteLandingsAutoUpdater
import cz.thomask.nasameteoritelandings.ui.main.MeteoriteLandingsListFragment
import cz.thomask.nasameteoritelandings.ui.mapdetail.MapViewDetailFragment
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [SubcomponentsModule::class, PreferencesModule::class, RealmConfigurationModule::class, OkHttpClientModule::class])
interface ApplicationComponent {
    fun inject(app: MeteoriteLandingsApplication)

    fun defaultPreferences(): SharedPreferences

    fun injectDatabaseAutoUpdater(updater: MeteoriteLandingsAutoUpdater)

    fun inject(updater: MainActivity)

    fun inject(updater: MeteoriteLandingsListFragment)

    fun inject(mapViewDetailFragment: MapViewDetailFragment)
}

@Module(subcomponents = [])
class SubcomponentsModule
