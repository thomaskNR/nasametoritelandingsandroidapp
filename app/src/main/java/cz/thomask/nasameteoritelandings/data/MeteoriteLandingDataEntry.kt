package cz.thomask.nasameteoritelandings.data

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonClass
import com.squareup.moshi.ToJson
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter

// Limitation of realm -> it can't be immutable data class
@RealmClass
open class MeteoriteLandingDataEntry() : RealmModel {
    var name: String = ""

    @PrimaryKey
    var id: String = ""
    var mass: Double? = null
    var year: Long? = null
    var reclat: Double? = null
    var reclon: Double? = null


    constructor(name: String, id: String, mass: Double?, year: Long?, reclat: Double?, reclon: Double?) : this() {
        this.name = name
        this.id = id
        this.mass = mass
        this.year = year
        this.reclat = reclat
        this.reclon = reclon
    }


    override fun toString(): String {
        return "MeteoriteLandingDataEntry(name='$name', id='$id', mass=$mass, year=${this.getYearAsString()}, reclat=$reclat, reclon=$reclon)"
    }


    fun getYearAsString(): String? {
        return if (year != null) {
            Instant.ofEpochMilli(year!!).toString()
        } else {
            null
        }
    }

    fun equalsContent(other: MeteoriteLandingDataEntry): Boolean {
        if (name != other.name) return false
        if (mass != other.mass) return false
        if (year != other.year) return false
        if (reclat != other.reclat) return false
        if (reclon != other.reclon) return false
        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MeteoriteLandingDataEntry

        if (id != other.id) return false

        return true
    }
}


// Mediate class needed for JSON parsing with Moshi
@JsonClass(generateAdapter = true)
data class MeteoriteLandingDataEntryJSON(
    val name: String,
    val id: String,
    val mass: Double?,
    val year: String?,
    val reclat: String?,
    val reclong: String?
)

// helper class for JSON parsing with Moshi - it allows transformation between JSON structure to Realm structure
class MeteoriteLandingDataEntryJsonAdapterImpl {
    @FromJson
    fun eventFromJson(jsonData: MeteoriteLandingDataEntryJSON): MeteoriteLandingDataEntry {
        val year: Long? = if (jsonData.year != null) {
            val dateTime = LocalDateTime.from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(jsonData.year))
            dateTime.toInstant(ZoneOffset.UTC).toEpochMilli()
        } else {
            null
        }
        val transformedData = MeteoriteLandingDataEntry(
            jsonData.name, jsonData.id, jsonData.mass, year,
            jsonData.reclat?.toDouble(), jsonData.reclong?.toDouble()
        )
        return transformedData
    }

    @ToJson
    fun eventToJson(transformedData: MeteoriteLandingDataEntry): MeteoriteLandingDataEntryJSON {
        val json = MeteoriteLandingDataEntryJSON(
            transformedData.name, transformedData.id, transformedData.mass,
            transformedData.getYearAsString(), transformedData.reclat?.toString(), transformedData.reclon?.toString()
        )
        return json
    }
}