package cz.thomask.nasameteoritelandings.data

import androidx.lifecycle.LiveData
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import cz.thomask.nasameteoritelandings.BuildConfig
import cz.thomask.nasameteoritelandings.SingleLiveEvent
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.threeten.bp.LocalDate
import timber.log.Timber
import java.io.IOException
import java.lang.reflect.Type
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MeteoriteLandingsRepository @Inject constructor(val okHttpClient: OkHttpClient) {

    fun downloadMeteoriteData(serverQuery: ServerQuery): Response {
        Timber.d("Request starting %s", serverQuery)
        return getResponseFromServer(urlForServerQuery(serverQuery))
    }

    fun urlForServerQuery(serverQuery: ServerQuery) = when (serverQuery) {
        ServerQuery.ALL -> "https://data.nasa.gov/resource/y77d-th95.json?\$limit=100000"
        ServerQuery.SINCE_2010 -> "https://data.nasa.gov/resource/gh4g-9sfh.json?\$where=year>'2010-01-01T00:00:00.000' limit 50000"
        ServerQuery.SINCE_2011 -> "https://data.nasa.gov/resource/gh4g-9sfh.json?\$where=year>'2011-01-01T00:00:00.000' limit 50000"
    }

    fun getResponseFromServer(url: String): Response {
        val request: Request = Request.Builder()
            .url(url)
            .addHeader("X-App-Token", BuildConfig.NASA_APP_KEY)
            .build()
        return okHttpClient.newCall(request).execute()
    }

    fun validateServerResponse(response: Response): RequestState {
        return if (response.isSuccessful) {
            Timber.d("Request finished ok ")
            RequestState.FINISHED_OK
        } else {
            Timber.d("Request failed %s", response.message)
            RequestState.FINISHED_FAILED_IO
        }
    }

    fun updateMeteoritesLandingsData(realm: Realm): RequestState {
        // check if update is not already in progress in other thread
        if (meteoritesUpdateStateLiveData.value == RequestState.STARTED) {
            return RequestState.ALREADY_RUNNING
        }
        meteoritesUpdateStateLiveData.postValue(RequestState.STARTED)
        // try to download raw data from server
        val serverResponse = downloadMeteoriteData(ServerQuery.SINCE_2010)
        // check if request to the server finished successfully
        if (validateServerResponse(serverResponse) == RequestState.FINISHED_OK) {
            // parse the data into internal representation of objects
            val parseResult = parseMeteoritesData(serverResponse.body!!.string())
            if (parseResult == null) {
                // parse failed, nothing more to do...
                meteoritesUpdateStateLiveData.postValue(RequestState.FINISHED_FAILED_PARSE)
                return RequestState.FINISHED_FAILED_PARSE
            } else {
                // parsed OK, save the data to the database
                replaceMeteoritesDataInDatabase(parseResult, realm)
                meteoritesUpdateStateLiveData.postValue(RequestState.FINISHED_OK)
                return RequestState.FINISHED_OK
            }
        } else {
            // server query failed, nothing more to do...
            meteoritesUpdateStateLiveData.postValue(RequestState.FINISHED_FAILED_IO)
            return RequestState.FINISHED_FAILED_IO
        }
    }

    fun replaceMeteoritesDataInDatabase(meteoritesInfoList: List<MeteoriteLandingDataEntry>, realm: Realm) {
        realm.executeTransaction { realm ->
            realm.delete(MeteoriteLandingDataEntry::class.java)
            Timber.d("Truncated table")
            realm.copyToRealm(meteoritesInfoList)
            Timber.d("Data copied to database")
        }
    }

    fun parseMeteoritesData(data: String): List<MeteoriteLandingDataEntry>? {
        return try {
            val moshi: Moshi = Moshi.Builder().add(MeteoriteLandingDataEntryJsonAdapterImpl()).build()
            val listMyData: Type = Types.newParameterizedType(MutableList::class.java, MeteoriteLandingDataEntry::class.java)
            val adapter: JsonAdapter<List<MeteoriteLandingDataEntry>> = moshi.adapter(listMyData)
            adapter.fromJson(data).also {
                Timber.d("Parsed OK %d", it!!.size)
            }
        } catch (ex: IOException) {
            Timber.i("JSONParse Exception")
            Timber.i(ex)
            null
        } catch (ex: JsonDataException) {
            Timber.i("JSONParse Data format Exception")
            Timber.i(ex)
            null
        }

    }

    fun queryDatabaseForFilteredData(filter: DataFilter, realm: Realm): RealmResults<MeteoriteLandingDataEntry> {
        val sortColumn = "mass"
        return when (filter) {
            DataFilter.ALL -> realm.where(MeteoriteLandingDataEntry::class.java).sort(sortColumn, Sort.DESCENDING).findAll()
            DataFilter.SINCE_2011 -> realm.where(MeteoriteLandingDataEntry::class.java)
                .greaterThanOrEqualTo("year", LocalDate.of(2011, 1, 1).toEpochDay())
                .sort(sortColumn, Sort.DESCENDING)
                .findAll()
            DataFilter.SINCE_2010 -> realm.where(MeteoriteLandingDataEntry::class.java)
                .greaterThanOrEqualTo("year", LocalDate.of(2010, 1, 1).toEpochDay())
                .sort(sortColumn, Sort.DESCENDING)
                .findAll()
        }
    }

    fun queryMeteoriteLandingInfoByID(id: String, realm: Realm): MeteoriteLandingDataEntry? {
        return realm.copyFromRealm(realm.where(MeteoriteLandingDataEntry::class.java).equalTo("id", id).findFirst())
    }

    fun getServerUpdateInfoStateLiveData(): LiveData<RequestState> {
        return meteoritesUpdateStateLiveData
    }

    companion object {
        private val meteoritesUpdateStateLiveData = SingleLiveEvent<RequestState>(RequestState.IDLE)
    }
}

enum class RequestState {
    IDLE, STARTED, FINISHED_OK, FINISHED_FAILED_IO, FINISHED_FAILED_PARSE, ALREADY_RUNNING
}

enum class ServerQuery {
    ALL, SINCE_2010, SINCE_2011
}

enum class DataFilter {
    ALL, SINCE_2010, SINCE_2011
}
