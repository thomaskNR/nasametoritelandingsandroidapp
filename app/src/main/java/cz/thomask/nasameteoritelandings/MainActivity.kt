package cz.thomask.nasameteoritelandings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.Snackbar
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import cz.thomask.nasameteoritelandings.data.RequestState
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var meteoritesRepository: MeteoriteLandingsRepository

    lateinit var appBarConfiguration: AppBarConfiguration

    private var serverUpdateProgressSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getApplicationComponent().inject(this)

        setContentView(R.layout.main_activity)
        // initialize Navigation component for this activity
        appBarConfiguration = AppBarConfiguration(mainNavController.graph)
        setupActionBarWithNavController(mainNavController, appBarConfiguration)

        meteoritesRepository.getServerUpdateInfoStateLiveData().observe(this, Observer { serverStatus ->
            showSnackbarWithCorrectServerStatusInfo(serverStatus)
        })
    }


    private fun showSnackbarWithCorrectServerStatusInfo(serverStatus: RequestState?) {
        when (serverStatus) {
            RequestState.IDLE -> serverUpdateProgressSnackbar?.dismiss()
            RequestState.STARTED -> {
                val text = getString(R.string.snackbar_update_of_database_is_in_progress)
                changeCurrentSnackbarTextAndDuration(text, Snackbar.LENGTH_INDEFINITE)
            }
            RequestState.FINISHED_OK -> {
                val text = getString(R.string.snackbar_update_of_database_finished)
                changeCurrentSnackbarTextAndDuration(text)
            }
            RequestState.FINISHED_FAILED_IO, RequestState.FINISHED_FAILED_PARSE -> {
                val text = getString(R.string.snackbar_update_of_database_failed)
                changeCurrentSnackbarTextAndDuration(text)
            }
            RequestState.ALREADY_RUNNING -> Unit
        }
    }

    private fun changeCurrentSnackbarTextAndDuration(text: String, duration: Int = Snackbar.LENGTH_LONG) {
        if (serverUpdateProgressSnackbar == null) {
            serverUpdateProgressSnackbar = Snackbar.make(findViewById(android.R.id.content), text, duration)
        }
        serverUpdateProgressSnackbar!!.apply {
            setText(text)
            setDuration(duration)
            show()
        }
    }

    val mainNavController: NavController
        get() {
            return findNavController(R.id.nav_host_fragment)
        }


    override fun onSupportNavigateUp(): Boolean {
        return mainNavController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
