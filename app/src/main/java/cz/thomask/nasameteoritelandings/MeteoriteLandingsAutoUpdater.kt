package cz.thomask.nasameteoritelandings

import android.content.Context
import androidx.work.*
import cz.thomask.nasameteoritelandings.Constants.WORKMANAGER_DATABASEUPDATE_WORK_KEY
import cz.thomask.nasameteoritelandings.Constants.WORKMANAGER_DATA_KEY_IMMEDIATE_DBSYNC
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import cz.thomask.nasameteoritelandings.data.RequestState
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MeteoriteLandingsAutoUpdater(val appContext: Context, val workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    @Inject
    lateinit var meteoritesRepository: MeteoriteLandingsRepository


    override fun doWork(): Result {
        appContext.getApplicationComponent().injectDatabaseAutoUpdater(this)

        var result: Result = Result.failure()

        val isAutoupdaterLogMessage = if (workerParams.inputData.getBoolean(WORKMANAGER_DATA_KEY_IMMEDIATE_DBSYNC, false)) {
            "IMMEDIATE_UPDATER"
        } else {
            "AUTOUPDATER"
        }
        Timber.d("%s STARTED ", isAutoupdaterLogMessage)
        RealmProvider.provideNewRealmInstance().use {
            val updateResult = meteoritesRepository.updateMeteoritesLandingsData(it)
            when (updateResult) {
                RequestState.FINISHED_FAILED_IO -> result = Result.retry()
                RequestState.FINISHED_FAILED_PARSE -> result = Result.failure()
                RequestState.FINISHED_OK -> result = Result.success()
                RequestState.ALREADY_RUNNING -> {
                    // another instance of updating is already running, no need to do it immediately once again
                    result = Result.success()
                }
                //not possible to achieve
                RequestState.IDLE, RequestState.STARTED -> Unit
            }
            Timber.d("%s %s", isAutoupdaterLogMessage, updateResult.name)
        }
        Timber.d("%s FINISHED", isAutoupdaterLogMessage)
        return result
    }

    companion object {
        private val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        fun createWorkRequestForPeriodicAutoUpdate(): PeriodicWorkRequest {
            return PeriodicWorkRequestBuilder<MeteoriteLandingsAutoUpdater>(1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .setInitialDelay(5, TimeUnit.MINUTES)
                .setInputData(workDataOf(WORKMANAGER_DATA_KEY_IMMEDIATE_DBSYNC to false))
                .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 5, TimeUnit.MINUTES)
                .build()
        }

        fun schedulePeriodicAutoupdate(context: Context) {
            WorkManager.getInstance(context.applicationContext)
                .enqueueUniquePeriodicWork(
                    WORKMANAGER_DATABASEUPDATE_WORK_KEY,
                    ExistingPeriodicWorkPolicy.KEEP,
                    createWorkRequestForPeriodicAutoUpdate()
                )
        }

        fun updateDatabaseImmediately(context: Context) {
            val saveRequest = OneTimeWorkRequestBuilder<MeteoriteLandingsAutoUpdater>()
                .setConstraints(constraints)
                .setInputData(workDataOf(WORKMANAGER_DATA_KEY_IMMEDIATE_DBSYNC to true))
                .build()
            WorkManager.getInstance(context.applicationContext).enqueue(saveRequest)
        }
    }

}


