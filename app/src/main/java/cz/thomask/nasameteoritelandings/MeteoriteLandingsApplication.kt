package cz.thomask.nasameteoritelandings

import android.app.Application
import android.content.Context
import androidx.core.content.edit
import com.jakewharton.threetenabp.AndroidThreeTen
import cz.thomask.nasameteoritelandings.di.ApplicationComponent
import cz.thomask.nasameteoritelandings.di.DaggerApplicationComponent
import cz.thomask.nasameteoritelandings.di.PreferencesModule
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber
import timber.log.Timber.DebugTree
import javax.inject.Inject


open class MeteoriteLandingsApplication : Application() {


    @Inject
    lateinit var realmConfiguration: RealmConfiguration

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        // Initialize realm database, needs to be called before injecting due to realmConfiguration
        Realm.init(this)

        setupDagger()

        initializeLibraries()

        MeteoriteLandingsAutoUpdater.schedulePeriodicAutoupdate(this)
        checkAndPerformUpdateOfDatabseAfterFreshInstallation()
    }

    open fun setupDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
            .preferencesModule(PreferencesModule(this))
            .build()
            .also { it.inject(this) }
    }

    private fun initializeLibraries() {
        // init java.time library
        AndroidThreeTen.init(this)

        // init logging tool in debug
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        Realm.setDefaultConfiguration(realmConfiguration)
    }


    private fun checkAndPerformUpdateOfDatabseAfterFreshInstallation() {
        // at first database is empty, update once after fresh install so that database will get ready for first use
        val preferences = applicationComponent.defaultPreferences()
        if (preferences.getBoolean(Constants.PREF_KEY_FIRST_DB_UPDATE_DONE, false) == false) {
            preferences.edit {
                putBoolean(Constants.PREF_KEY_FIRST_DB_UPDATE_DONE, true)
            }
            MeteoriteLandingsAutoUpdater.updateDatabaseImmediately(this)
        }
    }
}

fun Context.getApplication(): MeteoriteLandingsApplication {
    return (this.applicationContext as MeteoriteLandingsApplication)
}

fun Context.getApplicationComponent(): ApplicationComponent {
    return (this.applicationContext as MeteoriteLandingsApplication).applicationComponent
}
