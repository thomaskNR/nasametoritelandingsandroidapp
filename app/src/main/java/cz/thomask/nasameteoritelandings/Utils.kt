package cz.thomask.nasameteoritelandings

import androidx.lifecycle.LiveData
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmModel
import io.realm.RealmResults

class RealmLiveData<T : RealmModel>(private val results: RealmResults<T>) : LiveData<RealmResults<T>>() {
    private val listener: RealmChangeListener<RealmResults<T>> = RealmChangeListener { results -> value = results }

    init {
        if (results.isValid && results.isLoaded) {
            value = results
        }
    }

    override fun onActive() {
        results.addChangeListener(listener)
    }

    override fun onInactive() {
        results.removeChangeListener(listener)
    }

}

// Easier to use it outside of dagger injection due to open/closing of its instances (it could be solved by scoping in dagger...)
object RealmProvider {

    fun provideNewRealmInstance(): Realm {
        return Realm.getDefaultInstance()
    }
}

// Helper extensions
fun <T : RealmModel> RealmResults<T>.asLiveData() = RealmLiveData<T>(this)

fun Double.formatAndRoundToDigits(digits: Int) = "%.${digits}f".format(this)

object Constants {
    val PREF_KEY_FIRST_DB_UPDATE_DONE = "FIRST_DB_UPDATE_DONE"
    val PREF_KEY_SHOWED_WELCOME_SCREEN = "SHOWED_WELCOME_SCREEN"
    val WORKMANAGER_DATA_KEY_IMMEDIATE_DBSYNC = "IMMEDIATE"
    val WORKMANAGER_DATABASEUPDATE_WORK_KEY = "METEOR_DATABASE_AUTOUPDATE"
}