package cz.thomask.nasameteoritelandings

import dagger.Module
import dagger.Provides
import io.realm.RealmConfiguration
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import javax.inject.Singleton

@Module
class TestOkHttpClientModule {

    @Singleton
    @Provides
    fun defaultOkHttpClient() = OkHttpClient.Builder().addInterceptor(MockInterceptor()).build()
}

@Module
object TestRealmConfigurationModule {

    @Singleton
    @Provides
    fun defaultRealmConfiguration() = RealmConfiguration.Builder()
        .name("testDatabase")
        .schemaVersion(1)
        .inMemory()
        .build()
}

class MockInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        if (BuildConfig.DEBUG) {
            val response = urlsAndResponses[chain.request().url.toUri().toString()]
            return if (response != null) {
                Response.Builder()
                    .request(chain.request())
                    .code(200)
                    .protocol(Protocol.HTTP_2)
                    .message(response)
                    .body(
                        response.toByteArray().toResponseBody("application/json".toMediaTypeOrNull())
                    )
                    .addHeader("content-type", "application/json")
                    .build()
            } else {
                chain.proceed(chain.request())
            }
        } else {
            //just to be on safe side.
            throw IllegalAccessError(
                "MockInterceptor is only meant for Testing Purposes and " +
                        "bound to be used only with DEBUG mode"
            )
        }
    }

    companion object {
        private val urlsAndResponses = mutableMapOf<String, String>()
        fun addUrlAndResponse(url: String, response: String) {
            urlsAndResponses[url.toHttpUrl().toString()] = response
        }

        fun removeUrl(url: String) {
            urlsAndResponses.remove(url.toHttpUrl().toString())
        }
    }
}