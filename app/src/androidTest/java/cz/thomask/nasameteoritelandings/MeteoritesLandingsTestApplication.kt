package cz.thomask.nasameteoritelandings

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import cz.thomask.nasameteoritelandings.di.PreferencesModule
import cz.thomask.nasameteoritelandings.di.SubcomponentsModule
import dagger.Component
import javax.inject.Singleton


class MeteoritesLandingsTestApplication : MeteoriteLandingsApplication() {

    override fun setupDagger() {
        applicationComponent = DaggerTestApplicationComponent.builder()
            .preferencesModule(PreferencesModule(this))
            .build()
            .also { it.inject(this) }
    }
}

@Singleton
@Component(modules = [SubcomponentsModule::class, PreferencesModule::class, TestRealmConfigurationModule::class, TestOkHttpClientModule::class])
interface TestApplicationComponent : cz.thomask.nasameteoritelandings.di.ApplicationComponent {
    fun inject(repositoryInstrumentedTests: DataRepositoryInstrumentedTests)
}

class CustomTestRunner : AndroidJUnitRunner() {
    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, MeteoritesLandingsTestApplication::class.java.getName(), context)
    }
}