package cz.thomask.nasameteoritelandings

import android.content.Context
import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.work.Configuration
import androidx.work.ListenableWorker
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.testing.SynchronousExecutor
import androidx.work.testing.TestListenableWorkerBuilder
import androidx.work.testing.WorkManagerTestInitHelper
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingDataEntry
import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import cz.thomask.nasameteoritelandings.data.RequestState
import cz.thomask.nasameteoritelandings.data.ServerQuery
import io.realm.Realm
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import timber.log.Timber
import javax.inject.Inject


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class DataRepositoryInstrumentedTests {

    @Inject
    lateinit var meteoriteLandingsRepository: MeteoriteLandingsRepository

    lateinit var context: Context

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext.getApplication()
        ((context as MeteoritesLandingsTestApplication).applicationComponent as TestApplicationComponent).inject(this)
    }

    @After
    fun tearDown() {
    }


    // long running test - downloads real batch of data from the remote server (validation NASA API key is OK, that API endpoint is still
    // running, DB can handle big number of items, items json format is still the same,...)
    @Test
    fun downloadMeteoriteDataAndSaveToDatabase() {
        downloadAndTestSaveToRealm(ServerQuery.ALL)
    }

    private fun downloadAndTestSaveToRealm(serverQuery: ServerQuery) {
        meteoriteLandingsRepository.apply {
            Realm.getDefaultInstance().use {
                Timber.d("QUERY: %s", serverQuery)
                val response = downloadMeteoriteData(serverQuery)
                assertTrue(validateServerResponse(response) == RequestState.FINISHED_OK)
                val parsedResponse = parseMeteoritesData(response.body!!.string())
                assertTrue(parsedResponse != null)
                replaceMeteoritesDataInDatabase(parsedResponse!!, it)
                assertTrue(it.where(MeteoriteLandingDataEntry::class.java).findAll().size == parsedResponse.size)
            }
        }
    }

    @Test
    fun testUpdateWorkerPerformsUpdate() {
        mockAllUrls()
        val worker = TestListenableWorkerBuilder<MeteoriteLandingsAutoUpdater>(context).build()
        runBlocking {
            val result = worker.doWork()
            assertThat(result, `is`(ListenableWorker.Result.success()))
        }
        removeAllUrlMocks()
    }

    @Test
    @Throws(Exception::class)
    fun testPeriodicWorkerForAutoUpdateOfDatabase() {
        mockAllUrls()
        // Create request
        val request = MeteoriteLandingsAutoUpdater.createWorkRequestForPeriodicAutoUpdate()
        val config = Configuration.Builder()
            // Set log level to Log.DEBUG to make it easier to debug
            .setMinimumLoggingLevel(Log.DEBUG)
            .setExecutor(SynchronousExecutor())
            .build()
        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)

        val workManager = WorkManager.getInstance(context)
        val testDriver = WorkManagerTestInitHelper.getTestDriver(context)!!
        // Enqueue and wait for result.
        workManager.enqueue(request).result.get()
        // Tells the testing framework the period delay is met
        testDriver.setPeriodDelayMet(request.id)
        testDriver.setInitialDelayMet(request.id)
        testDriver.setAllConstraintsMet(request.id)
        // Get WorkInfo and outputData
        val workInfo = workManager.getWorkInfoById(request.id).get()
        // Assert
        assertThat(workInfo.state, `is`(WorkInfo.State.ENQUEUED))

        removeAllUrlMocks()
    }

    @Test
    fun downloadDataFromMockServerAndTestServerFailure() {
        val server = MockWebServer()
        // Schedule some responses.
        server.enqueue(MockResponse().setBody(mockResponse))
        server.enqueue(MockResponse().setResponseCode(502))
        server.start()
        val baseUrl = server.url("/meteoriteData")
        Timber.d("Starting mock server: %s", baseUrl)
        //test response ok
        meteoriteLandingsRepository.apply {
            Realm.getDefaultInstance().use {
                Timber.d("QUERY: %s", baseUrl)
                val response = getResponseFromServer(baseUrl.toString())
                assertTrue(validateServerResponse(response) == RequestState.FINISHED_OK)
                val parsedResponse = parseMeteoritesData(response.body!!.string())
                assertTrue(parsedResponse != null)
                replaceMeteoritesDataInDatabase(parsedResponse!!, it)
                assertTrue(Realm.getDefaultInstance().where(MeteoriteLandingDataEntry::class.java).findAll().size == 8)
            }
        }
        // test correct URL and that nasa app token was appended to the header
        val request1 = server.takeRequest()
        assertEquals("/meteoriteData", request1.path)
        assertNotNull(request1.getHeader("X-App-Token"))


        //test failure in response
        meteoriteLandingsRepository.apply {
            Timber.d("QUERY: %s", baseUrl)
            val response = getResponseFromServer(baseUrl.toString())
            assertTrue(validateServerResponse(response) == RequestState.FINISHED_FAILED_IO)
        }
        // Shut down the server. Instances cannot be reused.
        server.shutdown()
    }

    private fun mockAllUrls() {
        MockInterceptor.addUrlAndResponse(meteoriteLandingsRepository.urlForServerQuery(ServerQuery.SINCE_2011), mockResponse)
        MockInterceptor.addUrlAndResponse(meteoriteLandingsRepository.urlForServerQuery(ServerQuery.SINCE_2010), mockResponse)
        MockInterceptor.addUrlAndResponse(meteoriteLandingsRepository.urlForServerQuery(ServerQuery.ALL), mockResponse)
    }

    private fun removeAllUrlMocks() {
        MockInterceptor.removeUrl(meteoriteLandingsRepository.urlForServerQuery(ServerQuery.SINCE_2011))
        MockInterceptor.removeUrl(meteoriteLandingsRepository.urlForServerQuery(ServerQuery.SINCE_2010))
        MockInterceptor.removeUrl(meteoriteLandingsRepository.urlForServerQuery(ServerQuery.ALL))
    }

}


val mockResponse = """
[ {
    "name": "Boumdeid (2011)",
    "id": "57167",
    "nametype": "Valid",
    "recclass": "L6",
    "mass": "3599",
    "fall": "Fell",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "17.174930",
    "reclong": "-11.341330",
    "geolocation": {
    "latitude": "17.17493",
    "longitude": "-11.34133"
}
},
{
    "name": "Sołtmany",
    "id": "53829",
    "nametype": "Valid",
    "recclass": "L6",
    "mass": "1066",
    "fall": "Fell",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "54.008830",
    "reclong": "22.005000",
    "geolocation": {
    "latitude": "54.00883",
    "longitude": "22.005"
}
},
{
    "name": "Thika",
    "id": "54493",
    "nametype": "Valid",
    "recclass": "L6",
    "mass": "14200",
    "fall": "Fell",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "-1.002780",
    "reclong": "37.150280",
    "geolocation": {
    "latitude": "-1.00278",
    "longitude": "37.15028"
}
},
{
    "name": "Tissint",
    "id": "54823",
    "nametype": "Valid",
    "recclass": "Martian (shergottite)",
    "mass": "7000",
    "fall": "Fell",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "29.481950",
    "reclong": "-7.611230",
    "geolocation": {
    "latitude": "29.48195",
    "longitude": "-7.61123"
}
},
{
    "name": "Alkali Flat",
    "id": "55662",
    "nametype": "Valid",
    "recclass": "L5",
    "mass": "28",
    "fall": "Found",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "32.306000",
    "reclong": "-108.903800",
    "geolocation": {
    "latitude": "32.306",
    "longitude": "-108.9038"
}
},
{
    "name": "Anthony Gap",
    "id": "56391",
    "nametype": "Valid",
    "recclass": "L6",
    "mass": "347.36",
    "fall": "Found",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "32.005840",
    "reclong": "-106.560280",
    "geolocation": {
    "latitude": "32.00584",
    "longitude": "-106.56028"
}
},
{
    "name": "Benešov (a)",
    "id": "54854",
    "nametype": "Valid",
    "recclass": "LL3.5",
    "mass": "9.7200000000000006",
    "fall": "Found",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "49.766670",
    "reclong": "14.633330",
    "geolocation": {
    "latitude": "49.76667",
    "longitude": "14.63333"
}
},
{
    "name": "Benešov (b)",
    "id": "54855",
    "nametype": "Valid",
    "recclass": "H5",
    "mass": "1.54",
    "fall": "Found",
    "year": "2011-01-01T00:00:00.000",
    "reclat": "49.766670",
    "reclong": "14.633330",
    "geolocation": {
    "latitude": "49.76667",
    "longitude": "14.63333"
    }
} ]
"""

