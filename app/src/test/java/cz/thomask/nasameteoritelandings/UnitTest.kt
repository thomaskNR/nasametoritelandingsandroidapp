package cz.thomask.nasameteoritelandings

import cz.thomask.nasameteoritelandings.data.MeteoriteLandingsRepository
import cz.thomask.nasameteoritelandings.data.RequestState
import cz.thomask.nasameteoritelandings.data.ServerQuery
import cz.thomask.nasameteoritelandings.di.OkHttpClientModule
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UnitTest {

    @Before
    fun setUp() {

    }


    @Test
    fun downloadMeteoriteData() {
        MeteoriteLandingsRepository(OkHttpClientModule().defaultOkHttpClient()).apply {
            val response = downloadMeteoriteData(ServerQuery.SINCE_2011)
            Assert.assertTrue(validateServerResponse(response) == RequestState.FINISHED_OK)
            val parsedResponse = parseMeteoritesData(response.body!!.string())
            Assert.assertTrue(parsedResponse != null)
        }
    }


}
